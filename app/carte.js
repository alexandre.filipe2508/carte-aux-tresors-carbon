const { cellType } = require('./actions.js');

const createCarte = ({ width, height }) => {
  const map = new Array(height).fill(0).map(
    elem =>
      (elem = new Array(width).fill(0).map(cell => ({
        hasAventurier: false,
        tresor: 0
      })))
  );
  return map;
};

const carteMontagne = (value, dataCarte) => {
  const [, axeHorizontal, axeVertival] = value;
  const montagne = {
    axeHorizontal: +axeHorizontal,
    axeVertival: +axeVertival
  };
  dataCarte.montagnes.push(montagne);
  dataCarte.carte[axeVertival][axeHorizontal].type = cellType.montagne;
};

const carteTresor = (value, dataCarte) => {
  const [, axeHorizontal, axeVertival, count] = value;
  const tresor = {
    axeHorizontal: +axeHorizontal,
    axeVertival: +axeVertival,
    count: +count
  };
  dataCarte.tresors.push(tresor);
  dataCarte.carte[axeVertival][axeHorizontal].tresor = tresor;
};

const carteAventurier = (value, dataCarte) => {
  const [, name, axeHorizontal, axeVertival, orientation, sequenceString] = value;
  const sequence = sequenceString.split('');
  const aventurier = {
    name,
    axeHorizontal: +axeHorizontal,
    axeVertival: +axeVertival,
    orientation,
    sequence,
    tresorCount: 0
  };
  dataCarte.aventuriers.push(aventurier);
  dataCarte.carte[+axeVertival][+axeHorizontal].hasAventurier = true;

  if (sequence.length > dataCarte.lengthCarte) {
    dataCarte.lengthCarte = sequence.length;
  }
};

const carteSizeCarte = (value, dataCarte) => {
  const [ ,width, height] = value;
  dataCarte.carteSize.width = +width;
  dataCarte.carteSize.height = +height;
};

const carteTresors = ({ width, height, data, dataCarte }) => {
  dataCarte.carte = createCarte({ width, height });

  data.forEach(line => {
    const value = line.split(' - ');
    switch (value[0]) {
      case 'C':
        carteSizeCarte(value, dataCarte);
        break;
      case 'M':
        carteMontagne(value, dataCarte);
        break;
      case 'T':
        carteTresor(value, dataCarte);
        break;
      case 'A':
        carteAventurier(value, dataCarte);
        break;
    }
  });
};

module.exports = {
  carteTresors
};
