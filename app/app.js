const fs = require('fs');
const path = require('path');
const { storages } = require('./storages.js');
const { carteTresors } = require('./carte.js');
const { collecte } = require('./actions.js');
const { createOutputTxt } = require('./output.js');

const start = () => {
  const data = fs
    .readFileSync(path.join(process.cwd(), 'data.txt'), 'utf-8')
    .split('\n');
  const [, width, height] = data.find(elem => elem[0] === 'C').split('-');

  carteTresors({ width: +width, height: +height, data, dataCarte: storages.dataCarte });

  while (storages.dataCarte.currentCarte < storages.dataCarte.lengthCarte) {
    storages.dataCarte.aventuriers = collecte({
      aventuriers: storages.dataCarte.aventuriers,
      index: storages.dataCarte.currentCarte,
      map: storages.dataCarte.carte
    });
    storages.dataCarte.currentCarte++;
  }

  const output = createOutputTxt(storages.dataCarte);

  fs.writeFileSync(path.join(process.cwd(), 'output.txt'), output);

  console.log('Le fichier output.txt a bien été créé.');
};

module.exports.start = start;
