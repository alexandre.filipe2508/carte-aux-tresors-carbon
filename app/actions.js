const Orientations = {
  Nord: 'N',
  Sud: 'S',
  Est: 'E',
  Ouest: 'O'
};

const cellType = {
  montagne: 'M',
};

const newOrientation = ({ orientation, rotation }) => {
  if (rotation === 'D' || rotation === 'G') {
    switch (orientation) {
      case Orientations.Nord:
        return rotation === 'D' ? Orientations.Est : Orientations.Ouest;
      case Orientations.Sud:
        return rotation === 'D' ? Orientations.Ouest : Orientations.Est;
      case Orientations.Est:
        return rotation === 'D' ? Orientations.Sud : Orientations.Nord;
      case Orientations.Ouest:
        return rotation === 'D' ? Orientations.Nord : Orientations.Sud;
    }
    return orientation;
  }
};

const newPosition = (aventurier, map) => {
  const { orientation, axeHorizontal, axeVertival } = aventurier;
  const width = map[0].length;
  const height = map.length;
  switch (orientation) {
    case Orientations.Nord:
      return { axeHorizontal, axeVertival: axeVertival - 1 > 0 ? axeVertival - 1 : 0 };
    case Orientations.Sud:
      const toto = { axeHorizontal, axeVertival: axeVertival + 1 >= height ? axeVertival : axeVertival + 1 };
      return toto;
    case Orientations.Est:
      return { axeVertival, axeHorizontal: axeHorizontal + 1 >= width ? axeHorizontal : axeHorizontal + 1 };
    case Orientations.Ouest:
      return { axeVertival, axeHorizontal: axeHorizontal - 1 > 0 ? axeHorizontal - 1 : 0 };
  }
};


const avancer = ({ aventurier, action, map }) => {
  if (action === 'A') {
    const { axeHorizontal, axeVertival } = newPosition(aventurier, map);
    return okCell(map[axeVertival][axeHorizontal]) ? { ...aventurier, axeHorizontal, axeVertival } : aventurier;
  } else {
    const { orientation } = aventurier;
    const myNewOrientation = newOrientation({
      orientation,
      rotation: action
    });
    return { ...aventurier, orientation: myNewOrientation };
  }
};

const okCell = cell => !(cell.type === cellType.montagne || cell.hasAventurier);


const collecte = ({ aventuriers, index, map }) =>
  aventuriers.map(aventurier => {
    const { axeHorizontal: previousX, axeVertival: previousY } = aventurier;
    const aventurierAfterMove = avancer({
      aventurier,
      action: aventurier.sequence[index],
      map
    });
    const { axeHorizontal: newX, axeVertival: newY } = aventurierAfterMove;

    const newCell = map[aventurierAfterMove.axeVertival][aventurierAfterMove.axeHorizontal];
    if (
      newCell.tresor &&
      newCell.tresor.count &&
      (newX !== previousX || newY !== previousY)
    ) {
      aventurierAfterMove.tresorCount++;
      newCell.tresor.count--;
    }
    return aventurierAfterMove;
  });

module.exports = {
  newOrientation,
  newPosition,
  Orientations,
  cellType,
  collecte
};
