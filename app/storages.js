module.exports.storages = {
  dataCarte: {
    carteSize: {
      width: 0,
      height: 0
    },
    aventuriers: [],
    montagnes: [],
    tresors: [],
    lengthCarte: 0,
    currentCarte: 0,
    carte: 0
  }
};
