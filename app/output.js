const createOutputTxt = dataCarte => {
  const { aventuriers, carteSize, montagnes, tresors } = dataCarte;

  const carteOutput = `C - ${carteSize.width} - ${carteSize.height}\n`;

  const montagnesOuput = montagnes.reduce((res, montagne) => {
    res += `M - ${montagne.axeHorizontal} - ${montagne.axeVertival}\n`;
    return res;
  },``);

  const consigneTresorsOutput = `# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}\n`;

  const tresorsOuput = tresors.reduce((res, tresor) => {
    res += tresor.count > 0 ? `T - ${tresor.axeHorizontal} - ${tresor.axeVertival} - ${tresor.count}\n` : '';
    return res;
  },``);

  const consigneAventuriersOuput = `# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}\n`;

  const aventuriersOuput = aventuriers.reduce((res, aventurier) => {
    res += `A - ${aventurier.name} - ${aventurier.axeHorizontal} - ${aventurier.axeVertival} - ${aventurier.orientation} - ${aventurier.tresorCount}\n`;
    return res;
  },``);


  return carteOutput + montagnesOuput + consigneTresorsOutput + tresorsOuput + consigneAventuriersOuput + aventuriersOuput;
};

module.exports = {
  createOutputTxt
};
